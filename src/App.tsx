import React, { useEffect, useState } from "react";
import Board from "./components/Board";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { ICells } from "./types";
import Button from "react-bootstrap/Button";

const App: React.FC = () => {
  const cardsData = ["🌍", "🦊", "🍐", "🐝", "♥️", "🍇", "🍉", "🍑"];

  const generateCardsData = (cardsData: string[]): ICells[] => {
    const doubledCardsData = [...cardsData, ...cardsData];

    const randomizedData = doubledCardsData.map((icon, index) => ({
      icon,
      id: index,
      isRevealed: false,
      finalRevealed: false,
    }));

    // Randomize the order
    for (let i = randomizedData.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [randomizedData[i], randomizedData[j]] = [
        randomizedData[j],
        randomizedData[i],
      ];
    }

    return randomizedData.map((e, i) => ({ ...e, id: i }));
  };
  const [isDone, setIsDone] = useState(false);
  const [shownCount, setShownCount] = React.useState<number>(0);
  const [cells, setCells] = useState(() => generateCardsData(cardsData));

  const handleCellClick = (id: number) => {
    const item = { ...cells[id] };
    setCells([
      ...cells.slice(0, id),
      { ...item, isRevealed: true },
      ...cells.slice(id + 1),
    ]);

    setShownCount(shownCount + 1);
  };

  useEffect(() => {
    if (shownCount === 2) {
      console.log(cells);
      let cardsRevealed = cells.filter((item) => item.isRevealed === true);
      if (cardsRevealed[0].icon === cardsRevealed[1].icon) {
        const cellsUpdate = cells.map((e) => {
          if (e.icon === cardsRevealed[0].icon || e.finalRevealed) {
            return { ...e, isRevealed: false, finalRevealed: true };
          } else {
            return { ...e, isRevealed: false, finalRevealed: false };
          }
        });
        setCells(cellsUpdate);
      } else {
        // chờ 1s
        setTimeout(() => {
          const cellsUpdate = cells.map((e) => {
            if (e.finalRevealed) {
              return { ...e, isRevealed: false, finalRevealed: true };
            } else {
              return { ...e, isRevealed: false, finalRevealed: false };
            }
          });
          setCells(cellsUpdate);
        }, 1000);
      }
      setShownCount(0);
    }
  }, [shownCount, cells]);

  const handleCheckFinish = () => {
    const isAllRevealed = cells.every((e) => e.finalRevealed);
    setIsDone(isAllRevealed);
  };

  useEffect(() => {
    handleCheckFinish();
  }, [cells]);

  return (
    <div className="App">
      <main className="pt-5">
        <div className="mb-4 mx-auto">
          <Button
            onClick={() => {
              setCells(generateCardsData(cardsData));
              setIsDone(false);
            }}
          >
            reset
          </Button>
        </div>

        {isDone ? (
          "You won 🎉"
        ) : (
          <Board cells={cells} handleCellClick={handleCellClick} />
        )}
      </main>
    </div>
  );
};

export default App;
