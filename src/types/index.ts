// Shape.ts
export type ICells = {
  icon: string;
  isRevealed: boolean;
  id: number;
  finalRevealed: boolean;
};

// Color.ts
export type Color = "red" | "green" | "blue";

// GameState.ts
export type GameState = "waiting" | "revealed" | "matched";
