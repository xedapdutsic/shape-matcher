import React from "react";
import "./Cell.css";
import Col from "react-bootstrap/Col";

interface CellProps {
  icon: string;
  isRevealed: boolean;
  finalRevealed: boolean;
  onClick: () => void;
}

const Cell: React.FC<CellProps> = ({
  finalRevealed,
  isRevealed,
  onClick,
  icon,
}: CellProps) => {
  return (
    <Col
      className={"cell"}
      onClick={() => {
        !isRevealed && onClick();
      }}
    >
      <div
        className={` ${isRevealed || finalRevealed ? "revealed" : "hidden"}`}
      >
        {icon}
      </div>
    </Col>
  );
};

export default Cell;
