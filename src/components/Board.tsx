import React from "react";
import Cell from "./Cell";
import "./Board.css";
import { ICells } from "../types";

interface GameBoardProps {
  cells: ICells[];
  handleCellClick: (id: number) => void;
}

const Board: React.FC<GameBoardProps> = ({ cells, handleCellClick }) => {
  return (
    <div className="container-board">
      <div className="board">
        {cells.map((cell) => (
          <Cell
            key={cell.id}
            icon={cell.icon}
            isRevealed={cell.isRevealed}
            finalRevealed={cell.finalRevealed}
            onClick={() => handleCellClick(cell.id)}
          />
        ))}
      </div>
    </div>
  );
};

export default Board;
